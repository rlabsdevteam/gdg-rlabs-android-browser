package org.rlabs.gdg.browser;

import android.app.Activity;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Main extends Activity implements OnClickListener {
	// Warning: DO NOT ATTEMPT TO REMOVE THIS WARNING MESSAGE OR DISCLAIMER
	// BELOW
	// Disclaimer: GDG RLabs do not take responsibility of what attendees do
	// with
	// this code. This code was written in the attempts to educate attendees
	// around how to utilize the Android Platform.

	// private variable is a safe place to keep data while you application is
	// running.
	private WebView myWebView = null;
	private EditText editText1 = null;
	private String default_text = "http://gdg.rlabs.org";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// Info: At a start of any Android Application is the 'OnCreate'
		// function. This is where apps begins their execution.
		super.onCreate(savedInstanceState);
		// You can to let it load a view
		setContentView(R.layout.main);
		// Info: We create our first function to do more code for us
		setupLayout();
	}

	private void setupLayout() {
		// Here like previous application we link the xml elements with a
		// corresponding Java variable
		myWebView = (WebView) findViewById(R.id.webView1);
		// Here we telling the webview can run with javascript enabled :)
		myWebView.getSettings().setJavaScriptEnabled(true);

		// We are setting up the the Webview client and we creating the
		// WebViewClient inline
		myWebView.setWebViewClient(new WebViewClient() {
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				// Here you noticed that this function hands a URL from the user
				// into the function that changes the actually View
				view.loadUrl(url);
				return true;
			}

			@Override
			public void onReceivedSslError(WebView view,
					SslErrorHandler handler, SslError error) {
				// An extra bit to ignore SSL certificate errors
				handler.proceed();
			}

			@Override
			public void onPageFinished(WebView webview, String url) {
				super.onPageFinished(webview, url);
			}
		});
		// Here we giving webview and setting more details on when it loads for
		// the first time

		// Here we telling it to turn on the zoom functions
		myWebView.getSettings().setSupportZoom(true);
		// Below we telling webview to show the zoom in and out controls
		myWebView.getSettings().setBuiltInZoomControls(true);
		// Here we telling it to load a URL when it starts for the first time :)
		myWebView.loadUrl(default_text);

		// Here we just like previous project needs to link the XML to the java
		// object we going to be using,
		Button button1 = (Button) findViewById(R.id.button1);
		// and adding a listener to when someone clicks on the load button
		button1.setOnClickListener(this);

		// Here we just like previous project needs to link the XML to the java
		// object we going to be using,
		editText1 = (EditText) findViewById(R.id.editText1);
	}

	@Override
	public void onClick(View v) {
		// Here we do the code when the load button is clicked

		// First we create a new String to store our url typed in by the user
		String text = null;
		if (editText1.getText().toString().length() > 0) {
			// Above we testing if the text from the editText holds a valid URL,
			// this code below first adds to the 'text' variable http:// so the
			// user dont need to
			text = "http://" + editText1.getText();
		} else {
			// Here we telling the user to type in a valid URL, cause they didnt
			// :(
			Toast.makeText(this, "Please type in a valid URL",
					Toast.LENGTH_LONG).show();
			text = default_text;
		}

		// After testing everything, we finally load either the correct or
		// default url and hand it to the webview
		myWebView.loadUrl(text);
	}
}
